function initAccordion(accordionElem){

    //when panel is clicked, handlePanelClick is called.          
    function handlePanelClick(event){
        showPanel(event.currentTarget.parentNode);
    }

    //Hide currentPanel and show new panel.  

    function showPanel(panel){
        //Show new one
        panel.classList.toggle("active");

    }

    var allPanelElems = accordionElem.querySelectorAll(".algol-has-children");
    for (var i = 0, len = allPanelElems.length; i < len; i++){
            allPanelElems[i].addEventListener("click", handlePanelClick);
    }

    //By Default Show first panel
    //showPanel(allPanelElems[0])

}
window.addEventListener('load', function(e) {
    initAccordion( document.getElementById("algol-accordion") )
});
