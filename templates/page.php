<?php
/**
 * 
 */
get_header(); ?>
<?php
    while ( have_posts() ) :
        the_post();
        load_template( ALGOL_ABSPATH . 'templates/parts/page-content.php', true ); 
    endwhile;
    ?>
<?php
get_footer();