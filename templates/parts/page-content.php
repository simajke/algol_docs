<?php //global $post; ?>
<div class="algol-container">
    <div class="algol-docs-template algol-docs-template_position" id="algol-docs-main">
        <div class="algol-docs-row">
            <div class="algol-docs__content-wrapper">
                <div class ="algol-docs__accordion-wrapper padding-default" data-sticky-container>
                    <!-- <div id="algol-accordion-wrapper"> -->
                        <?php load_template( ALGOL_ABSPATH . 'templates/parts/accordion.php', true ); ?>
                    <!-- </div> -->
                </div>
                <?php //$wrapper_class = 'algol-docs__categories-wrapper padding-default'; ?>
                <?php //$wrapper_class .= ( 'algol_docs' !== $post->post_type ) ? ' flex-row' : ''; ?>
                <div class="algol-docs__categories-wrapper padding-default">
                    <?php
                        //if ( 'algol_docs' !== $post->post_type ) { 
                        load_template( ALGOL_ABSPATH . 'templates/parts/main-block.php', true );
                        // } else {
                        //     if ( have_posts() ):
                        //         while ( have_posts() ):
                        //             the_post();
                        //             the_content();
                        //         endwhile;
                        //     else:
                        //         echo "The post is empty. Please come back later";
                        //     endif;
                        // }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>