<?php
/**
 * The template for displaying algol_docs post type pages.
 * @package storefront
 */
get_header(); ?>
<?php
    load_template( ALGOL_ABSPATH . 'templates/parts/page-content.php', true );
?>
<?php
get_footer();