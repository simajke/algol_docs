<?php
/**
 * Plugin Name: Algol Docs
 * Text Domain: algol_docs
 * Domain Path: /languages/
 * 
*/
/** add up '[algol_docs]' to page to make the plugin work  */
defined( 'ABSPATH' ) || exit;

if ( ! defined( 'ALGOL_PATH' ) ) {
	define( 'ALGOL_PATH', __FILE__ );
}

if ( !class_exists( 'Algol_Docs' ) ) {
    include_once plugin_dir_path( ALGOL_PATH ) . 'include/class-algol.php';
}
function algol_docs_main() {
    return Algol_Docs::get_instance();
}
$algol_class = algol_docs_main();