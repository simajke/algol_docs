<?php
abstract class Algol_Docs_Table extends WP_List_Table {
    public function get_columns(){
        $columns = array(
            'title' => 'Title',
            'author'    => 'Author',
            'date'      => 'Date'
        );
        return $columns;
    }
}