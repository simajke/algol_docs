<?php
class Algol_Walker extends Walker {

    public $db_fields = array(
        'parent' => 'menu_item_parent',
        'id'     => 'ID',
    );

    public $has_children;

    public function start_lvl( &$output, $depth = 0, $args = array() ) {

        $output .= "<ul class=\"algol-accordion__body algol-submenu algol-submenu-{$depth}\"";
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .=  "</ul>";
    }

    public function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
        $class = ( true === $this->has_children ) ? "class=\"algol-has-children\"" : "";
        $url = ( false === $this->has_children ) ? "href=\"{$object->url}\"" : "";
        $output .= "<li class=\"algol-accordion__header\"><a $class $url>{$object->title}</a>";
    }

    public function end_el( &$output, $object, $depth = 0, $args = array() ) {
        $output .= "</li>";
    }
}