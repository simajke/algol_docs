<?php
class Algol_Taxonomy_Manager {
    const ALGOL_ORDER_EXPORT                      = 'algol_order_export';
    const ALGOL_PRICING                           = 'algol_pricing';
    const ALGOL_PHONE_ORDER_TYPE                  = 'algol_phone_order';
    private static $default_taxonomies            = array(
                                                        'Order Export' => self::ALGOL_ORDER_EXPORT,
                                                        'Pricing'      => self::ALGOL_PRICING,
                                                        'Phone Order'  => self::ALGOL_PHONE_ORDER_TYPE,
                                                    );

    private static $default_taxonomies_shortcodes = array(
                                                        Algol_Post_Type_Manager::ALGOL_ORDER_EXPORT_TYPE => self::ALGOL_ORDER_EXPORT,
                                                        Algol_Post_Type_Manager::ALGOL_PRICING_TYPE      => self::ALGOL_PRICING,
                                                        Algol_Post_Type_Manager::ALGOL_PHONE_ORDER_TYPE  => self::ALGOL_PHONE_ORDER_TYPE,
                                                    );
    public static $all_taxonomies                 = array();

    public function __construct( ) {
        $defaults = self::$default_taxonomies;
        if ( ! empty( self::$all_taxonomies ) ) {
            foreach ( $defaults as $label => $tax ) {
                if ( ! in_array( $tax, self::$all_taxonomies ) ) {
                    self::$all_taxonomies[ $label ] = $tax;
                }
            }
            return;
        }
        self::$all_taxonomies = $defaults;
    }

    public function init() {
        if ( ! empty( self::$default_taxonomies ) ) {
            foreach( self::$default_taxonomies as $label_name => $taxonomy_name ){
                $this->default_taxonomiy_registration( $taxonomy_name, $label_name );
            }
        }
    }

    private function default_taxonomiy_registration( $taxonomy_name, $label_name ) {
        if ( false === taxonomy_exists( $taxonomy_name ) ) {
            $default_post_types = Algol_Post_Type_Manager::get_default_post_types();
            register_taxonomy( $taxonomy_name, [ $default_post_types[ $label_name ] ], [ 
                'label'                 => '',
                'labels'                => [
                    'name'              => sprintf( __( '%s Categories' , 'algol_docs' ) , $label_name ),
                    'singular_name'     => __( 'Category', 'algol_docs' ),
                    'search_items'      => __( 'Search Categories', 'algol_docs' ),
                    'all_items'         => __( 'All Categories', 'algol_docs' ),
                    'view_item '        => __( 'View Category', 'algol_docs' ),
                    'parent_item'       => __( 'Parent Category', 'algol_docs' ),
                    'parent_item_colon' => __( 'Parent Category:', 'algol_docs' ),
                    'edit_item'         => __( 'Edit Category', 'algol_docs' ),
                    'update_item'       => __( 'Update Category', 'algol_docs' ),
                    'add_new_item'      => sprintf( __( 'Add New %s Category' , 'algol_docs' ) , $label_name ),
                    'new_item_name'      => sprintf( __( 'New %s Category Name' , 'algol_docs' ) , $label_name ),
                    'new_item_name'      => sprintf( __( 'Add New %s Category' , 'algol_docs' ) , $label_name ),
                ],
                'description'           => '',
                'public'                => true,
                'hierarchical'          => true,
        
                'rewrite'               => true,
                'capabilities'          => array(),
                'meta_box_cb'           => null,
                'show_admin_column'     => false,
                'show_in_rest'          => null,
                'rest_base'             => null,
            ] );
        }
    }

    public static function get_default_taxonomies() {
        return self::$default_taxonomies;
    }

    public static function default_taxonomies_shortcodes() {
        return self::$default_taxonomies_shortcodes;
    } 

    public static function get_active_taxonomy( $post ) {
        $default_posttypes_shortcodes = self::$default_taxonomies_shortcodes;
        $active_post_type = Algol_Post_Type_Manager::get_active_default_posttype( $post );
        if ( isset( $default_posttypes_shortcodes[ $active_post_type ] ) ) {
            return $default_posttypes_shortcodes[ $active_post_type ];
        }
        return false;
    }
}