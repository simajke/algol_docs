<?php
class Algol_Post_Type_Manager {
    const ALGOL_ORDER_EXPORT_TYPE              = 'algol_order_export';
    const ALGOL_PRICING_TYPE                   = 'algol_pricing';
    const ALGOL_PHONE_ORDER_TYPE               = 'algol_phone_order';
    private static $active_default_posttype;        
    private static $default_post_types_created = false;
    private static $default_post_types         = array( 'Order Export' => self::ALGOL_ORDER_EXPORT_TYPE,
                                                        'Pricing'      => self::ALGOL_PRICING_TYPE,
                                                        'Phone Order'  => self::ALGOL_PHONE_ORDER_TYPE,
                                                    );

    public function __construct( ) {
        if( false === self::$default_post_types_created ) {
            foreach ( self::$default_post_types as $label => $post_type ) {
                    $this->register_default_post_type( $post_type, $label);
            }
            self::$default_post_types_created = true;
        }
    }

    public static function get_active_default_posttype( $post ) {
        $active_shortcode = Algol_Layout_Manager::get_active_default_shortcode( $post );
        $default_shortcodes = Algol_Layout_Manager::get_default_shortcodes();
        foreach ( $default_shortcodes as $post_type => $short_code) {
            if ( $active_shortcode === $short_code ) {
                self::$active_default_posttype = $post_type;
                return self::$active_default_posttype;
            }
        }
        return false;
    }
    private function register_default_post_type( $post_type, $label ) {
        if ( false === post_type_exists( $post_type ) ) {
            //$default_taxonomies = Algol_Taxonomy_Manager::get_default_taxonomies();
            register_post_type( $post_type, array(
                'labels'             => array(
                    'name'               => sprintf( __( 'Articles for %s' , 'algol_docs' ) , $label ),
                    'singular_name'      => __( 'New Article', 'algol_docs' ),
                    'add_new'            => __( 'Add New Article', 'algol_docs' ),
                    'add_new_item'       => __( 'Add New Article', 'algol_docs' ),
                    'edit_item'          => __( 'Edit Article', 'algol_docs' ),
                    'new_item'           => __( 'New Article', 'algol_docs' ),
                    'view_item'          => __( 'View an Article', 'algol_docs' ),
                    'search_items'       => __( 'Find Article', 'algol_docs' ),
                    'not_found'          =>  __( 'Articles not found', 'algol_docs' ),
                    'not_found_in_trash' => __( 'Articles not found in trash', 'algol_docs' ),
                    'parent_item_colon'  => '',
                    'menu_name'          => sprintf( __( '%s Documentation' , 'algol_docs' ) , $label ),
                    ),
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => true,
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => true,
                'menu_position'      => null,
                'supports'           => array( 'title','editor','author','thumbnail','excerpt', 'page-attributes' ),
                //'taxonomies'         => array( $default_taxonomies[ $label ] ),
            ) );
        }
    }

    public static function get_default_post_types() {
        return self::$default_post_types;
    }
}