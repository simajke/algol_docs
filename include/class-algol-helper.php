<?php
class Algol_Helper {
    public static function array_string_exists( $array_searchable, $content ) {
        foreach ( $array_searchable as $el ) {
            if ( false !== strpos( $content, $el ) ) {
                return true;
            }
        }
        return false;
    }

}