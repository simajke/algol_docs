<?php
defined( 'ABSPATH' ) || exit;

class Algol_Docs {
    protected static $_algol_instance = null;

    private function __construct() {
        $this->define_constants();
        $this->includes();
        $this->init_hooks();
    }

    public static function get_instance() {
        if ( null === self::$_algol_instance ) {
            self::$_algol_instance = new self();
        }
        return self::$_algol_instance;
    }

    private function define_constants() {
        define( ALGOL_ABSPATH, plugin_dir_path( ALGOL_PATH ) );
    }

    private function includes() {
        //classes
        include_once ALGOL_ABSPATH . 'include/class-algol-helper.php';
        include_once ALGOL_ABSPATH . 'include/class-algol-walker.php';
        include_once ALGOL_ABSPATH . 'include/class-algol-post-type-manager.php';
        include_once ALGOL_ABSPATH . 'include/class-algol-menus-manager.php';

        include_once ALGOL_ABSPATH . 'include/layouts/class-algol-layout-manager.php';
    }

    private function init_hooks() {
        // add_filter( 'single_template', function() {
        //     global $post;
        
        //     if ( 'algol_docs' === $post->post_type ) {
        //         $single_template = ALGOL_ABSPATH . 'templates/algol_docs-template.php';
        //     }
        
        //     return $single_template;
        // } );
        add_action( 'plugins_loaded', array( $this, 'load_translation' ) );
        add_action( 'init', array( $this, 'init' ) );
        add_action( 'wp', array( $this, 'redefine_template' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'admin_menu', array( $this, 'setup_plugin_settings' ) );
        add_action( 'wp_footer', array( $this, 'add_inline_scripts' ) );
    }
    public function load_translation() {
        load_plugin_textdomain( 'algol_docs', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }
    public function init() {
        ( new Algol_Post_Type_Manager() );
        ( new Algol_Menus_Manager() );
    }
    public function redefine_template() {
        ( new Algol_Layout_Manager() );
    }

    public function enqueue_scripts() {
        wp_enqueue_style( 'algol_style', plugins_url('assets/css/style.css', ALGOL_PATH) );
        wp_enqueue_style( 'algol_accordion_style', plugins_url('assets/css/accordion_style.css', ALGOL_PATH) );
        wp_enqueue_style( 'fontawesome', plugins_url('assets/css/fontawesome/css/font-awesome.css', ALGOL_PATH) );
        wp_enqueue_script('algol_script', plugins_url('assets/js/script.js', ALGOL_PATH) );
        wp_enqueue_script('algol_accordion_script', plugins_url('assets/js/accordion_script.js', ALGOL_PATH), array(), '1.0.0', true );
        wp_enqueue_script('sticky_algol_accordion', plugins_url('assets/sticky-js-master/sticky.min.js', ALGOL_PATH), array(), '1.0.0' );
    }

    public function add_inline_scripts() {
        ?>
            <script>
                var sticky = new Sticky( selector = "#algol-accordion", options = {
                    marginTop: 200
                } );
            </script>
        <?php
    }
}