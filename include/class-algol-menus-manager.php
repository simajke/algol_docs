<?php
class Algol_Menus_Manager {

    const DEFAULT_MENU_LOCATION_ID     = 'algol_sidebar';
    const DEFAULT_MENU_DESCRIPTION     = 'Algol Sidebar';
    private static $active_menu;
    private static $default_menu_names = array();
    public function __construct() {
        $this->init();
    }

    public function register_nav_menu( $location, $description ) {
        if ( false === wp_get_nav_menu_object( $location ) ) {
            register_nav_menu( $location, $description );
            return true;
        }
        return false;
    }

    public function create_nav_menu( $menu_name ) {
        if ( false === wp_get_nav_menu_object( $menu_name ) ) {
            wp_create_nav_menu( $menu_name );
            return true;
        }
        return false;
    }

    public static function get_default_menu_names() {
        return self::$default_menu_names;
    }

    private function init() {
        $this->register_nav_menu( self::DEFAULT_MENU_LOCATION_ID, self::DEFAULT_MENU_DESCRIPTION );
        $this->create_default_nav_menus(); 
    }

    private function create_default_nav_menus() {
        $default_post_types = Algol_Post_Type_Manager::get_default_post_types();
        foreach( $default_post_types as $post_type ) {
            $post_type_menu = ucfirst( str_replace('_', ' ', $post_type) );
            $post_type_menu .= ' menu';
            self::$default_menu_names[ $post_type ] = $post_type_menu;
            $this->create_nav_menu( $post_type_menu );
        }
    }

    public static function set_active_menu( $post ) {
        $menu_names = self::get_default_menu_names();
        $active_post_type = Algol_Post_Type_Manager::get_active_default_posttype( $post );
        if( isset( $menu_names[ $active_post_type ] ) ) {
            self::$active_menu = $menu_names[ $active_post_type ];
            return true;
        }
        return false;
    }
    public static function get_active_menu( $post ) {
        $menu_names = self::get_default_menu_names();
        $active_post_type = Algol_Post_Type_Manager::get_active_default_posttype( $post );
        if( isset( $menu_names[ $active_post_type ] ) ) {
            self::$active_menu = $menu_names[ $active_post_type ];
            return self::$active_menu;
        }
        return false;
    }
}