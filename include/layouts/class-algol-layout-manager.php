<?php
class Algol_Layout_Manager {
    private static $default_shortcodes            = array(
        Algol_Post_Type_Manager::ALGOL_ORDER_EXPORT_TYPE  => '[algol_order_export]',
        Algol_Post_Type_Manager::ALGOL_PRICING_TYPE       => '[algol_pricing]',
        Algol_Post_Type_Manager::ALGOL_PHONE_ORDER_TYPE   => '[algol_phone_order]',
            );
    private static $default_page_path             = ALGOL_ABSPATH . 'templates/page.php';  

    public function __construct() {
        $this->init();
    }

    private function init() {
        global $post;
        $content = $post->post_content;
        if ( true === Algol_Helper::array_string_exists( self::$default_shortcodes, $content ) ) {
            add_filter( 'page_template', function() {
                return ALGOL_ABSPATH . 'templates/page.php';
            } );
        }
    }

    //recieve active shortocode in case the content is equal to one of default shortcodes 
    public static function get_active_default_shortcode( $post ) {
        $content = $post->post_content;

        foreach ( self::$default_shortcodes as $post_type => $shortcode ) {
            if ( false !== strpos( $content, $shortcode ) ) {
                return $shortcode;
            }
        }
        return false;
    }

    public function get_default_page_template_path() {
        return self::$default_page_path;
    }

    public static function get_default_shortcodes() {
        return self::$default_shortcodes;
    }
    // private static $default_shortcodes_registered = false;
    // private static $default_layout_objects        = array();
    //private static $default_page_path             = ALGOL_ABSPATH . 'templates/page.php';

    // public function __construct() {
    //     $this->init();
    // }

    // private function init() {
    //     if (  ) {

    //     }
    //     // add_filter( 'page_template', function() {
    //     //     return ALGOL_ABSPATH . 'templates/page.php';
    //     // } );
    //     // $this->generate_layout_objects();
    //     //$this->register_default_shortcodes( self::$default_shortcodes );
    // }

    // public static function get_default_layout_objects() {
    //     return self::$default_layout_objects;
    // }

    // private function generate_layout_objects() {

    // }

    // private function register_default_shortcodes( array $shortcodes ) {
    //     if ( false === self::$default_shortcodes_registered && ! empty( $shortcodes ) ) {
    //         foreach( $shortcodes as $shortcode ) {
    //             if ( false === shortcode_exists( $shortcode ) ) {
    //                 //make words' first letters capital to invoke layout classes' methods on pages
    //                 $capital_letters = array_map ( 'ucwords', explode( "_", $shortcode ) );
    //                 $class_name      = implode( '_', $capital_letters );
    //                 $class_name     .= '_Layout';
    //                 if ( class_exists( $class_name ) ) {
    //                     $layout_object   = new $class_name();
    //                     self::$default_layout_objects[] = $layout_object;
    //                     //add_shortcode( $shortcode, array( $layout_object, 'get_page_layout' ) );
    //                 }
    //             }
    //         }
    //         self::$default_shortcodes_registered = true;
    //     }
    // }
}